var gulp = require('gulp'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	watch = require('gulp-watch'),
	livereload= require('gulp-livereload'),
	plumber = require('gulp-plumber'),
	sass = require('gulp-sass'),
	connect = require('gulp-connect'),
	changed = require('gulp-changed'),
	notify = require('gulp-notify'),
	growl = require('gulp-notify-growl');

gulp.task('connect', function() {
	connect.server({
		port: 8000,
		livereload: true
	});
});

var directory = 'assets/';

// Initialize the notifier
var growlNotifier = growl({
  hostname : '192.168.56.1' // IP or Hostname to notify, default to localhost
});

// Concatenate & Minify JS 
gulp.task('scripts', function() {
    return gulp.src(directory+'*.js')
        .pipe(watch({
            glob: directory+'*.js',
            emit: 'all',
            emitOnGlob: false
        }, function(files) {
            return files.pipe(plumber())
                .pipe(concat('main.js'))
                .pipe(uglify())
                .pipe(rename({
                    suffix: '.min'
                }))
                .pipe(gulp.dest(directory))
                // the error handler isn't really needed because this check for errors
                // so with each save you will get a success . Perhaps use jshint.
                .pipe(plumber({errorHandler: growlNotifier.onError('<%= error.message %>')}))
                .pipe(growlNotifier({
                    title: 'Success',
                    message: 'Compiled <%= file.relative %>'
                }))
                .pipe(connect.reload());
        }))        
});

gulp.task('sass', function() {
	return gulp.src(directory+'*.scss')
		.pipe(watch({
			glob: directory+'*.scss',
			emit: 'all',
			emitOnGlob: false
		}, function(files) {
			return files.pipe(plumber())
				.pipe(sass({
					errLogToConsole: true,
					outputStyle: 'compressed'
				}))
				.pipe(gulp.dest(directory));
		}))
		.pipe(plumber({errorHandler: growlNotifier.onError('<%= error.message %>')}))
		.pipe(changed(directory, {extension: '.css'}))
		.pipe(sass({
			errLogToConsole: true,
			outputStyle: 'compressed'
		}))
		.pipe(gulp.dest(directory))
		.pipe(growlNotifier({
			title: 'Success',
			message: 'Compiled <%= file.relative %>'
		}))
		.pipe(connect.reload());
});

// Watch HTML
gulp.task('html', function() {
  return gulp.src('*.html')
	.pipe(watch())
	.pipe(connect.reload());
});

// Default Task
gulp.task('default', ['connect','scripts','sass','html']);
